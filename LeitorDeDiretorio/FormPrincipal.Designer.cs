﻿namespace LeitorDeDiretorio
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnDiretorio = new System.Windows.Forms.Button();
            this.btnSalvarNomeArquivo = new System.Windows.Forms.Button();
            this.openFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.lblDiretorioSelcionado = new System.Windows.Forms.Label();
            this.lblDiretorioSave = new System.Windows.Forms.Label();
            this.openFolderBrowserSave = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(312, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Buscador de Arquivos";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnDiretorio
            // 
            this.btnDiretorio.Location = new System.Drawing.Point(26, 162);
            this.btnDiretorio.Name = "btnDiretorio";
            this.btnDiretorio.Size = new System.Drawing.Size(260, 62);
            this.btnDiretorio.TabIndex = 1;
            this.btnDiretorio.Text = "ABRIR DIRETÓRIO";
            this.btnDiretorio.UseVisualStyleBackColor = true;
            this.btnDiretorio.Click += new System.EventHandler(this.btnDiretorio_Click);
            // 
            // btnSalvarNomeArquivo
            // 
            this.btnSalvarNomeArquivo.Location = new System.Drawing.Point(509, 162);
            this.btnSalvarNomeArquivo.Name = "btnSalvarNomeArquivo";
            this.btnSalvarNomeArquivo.Size = new System.Drawing.Size(260, 62);
            this.btnSalvarNomeArquivo.TabIndex = 2;
            this.btnSalvarNomeArquivo.Text = "SALVAR RELATÓRIO";
            this.btnSalvarNomeArquivo.UseVisualStyleBackColor = true;
            this.btnSalvarNomeArquivo.Click += new System.EventHandler(this.btnSalvarNomeArquivo_Click);
            // 
            // lblDiretorioSelcionado
            // 
            this.lblDiretorioSelcionado.AutoSize = true;
            this.lblDiretorioSelcionado.Location = new System.Drawing.Point(267, 261);
            this.lblDiretorioSelcionado.Name = "lblDiretorioSelcionado";
            this.lblDiretorioSelcionado.Size = new System.Drawing.Size(19, 13);
            this.lblDiretorioSelcionado.TabIndex = 3;
            this.lblDiretorioSelcionado.Text = "...";
            // 
            // lblDiretorioSave
            // 
            this.lblDiretorioSave.AutoSize = true;
            this.lblDiretorioSave.Location = new System.Drawing.Point(267, 294);
            this.lblDiretorioSave.Name = "lblDiretorioSave";
            this.lblDiretorioSave.Size = new System.Drawing.Size(19, 13);
            this.lblDiretorioSave.TabIndex = 4;
            this.lblDiretorioSave.Text = "...";
            // 
            // FormPrincipal
            // 
            this.AcceptButton = this.btnSalvarNomeArquivo;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(793, 318);
            this.Controls.Add(this.lblDiretorioSave);
            this.Controls.Add(this.lblDiretorioSelcionado);
            this.Controls.Add(this.btnSalvarNomeArquivo);
            this.Controls.Add(this.btnDiretorio);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPrincipal";
            this.Padding = new System.Windows.Forms.Padding(23, 60, 23, 20);
            this.Resizable = false;
            this.Load += new System.EventHandler(this.FormPrincipal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDiretorio;
        private System.Windows.Forms.Button btnSalvarNomeArquivo;
        private System.Windows.Forms.FolderBrowserDialog openFolder;
        private System.Windows.Forms.Label lblDiretorioSelcionado;
        private System.Windows.Forms.Label lblDiretorioSave;
        private System.Windows.Forms.FolderBrowserDialog openFolderBrowserSave;
    }
}

