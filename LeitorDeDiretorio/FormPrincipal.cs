﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace LeitorDeDiretorio
{
    public partial class FormPrincipal : MetroForm
    {
        private String PathDoDiretorio     = null;
        private string PathDoDiretorioSave = null;

        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            
        }

        private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {

        }

        private void btnDiretorio_Click(object sender, EventArgs e)
        {
            openFolder.Description = "Selecione uma pasta para procurar os Arquivos";
            DialogResult resultado = openFolder.ShowDialog();

            if (resultado == DialogResult.OK) 
            {
                lblDiretorioSelcionado.Text = "Diretório Selecionado: "+ openFolder.SelectedPath;
                this.PathDoDiretorio        = openFolder.SelectedPath;
            }
        }

        private void SaveNameArquivos()
        {
            if (this.PathDoDiretorio == null)
            {
                MessageBox.Show("Por favor selecione um Diretório!",
                                "Atenção",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation,
                                MessageBoxDefaultButton.Button1);
            }
            else
            {
                try
                {
                    string[] arquivos = Directory.GetFiles(this.PathDoDiretorio, "*.pdf", SearchOption.AllDirectories);
                    DirectoryInfo di = new DirectoryInfo(this.PathDoDiretorio);

                    if (arquivos.Length == 0)
                    {
                        MessageBox.Show("Não há arquivos PDF neste Diretório!",
                                   "Atenção",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Exclamation,
                                   MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        openFolderBrowserSave.Description = "Selecione uma pasta para salvar!";
                        DialogResult resultado            = openFolderBrowserSave.ShowDialog();

                        if (resultado == DialogResult.OK)
                        {
                            lblDiretorioSave.Text = "Relatório salvo em : " + openFolderBrowserSave.SelectedPath;
                            this.PathDoDiretorioSave = openFolderBrowserSave.SelectedPath;
                        }
                      
                        StreamWriter writer;
                        string CaminhoNome =  string.Concat(openFolderBrowserSave.SelectedPath,"\\relatorio.txt");
                        writer             = File.CreateText(CaminhoNome);

                        foreach (var arq in di.GetFiles("*.pdf", SearchOption.AllDirectories))
                        {
                            writer.WriteLine(arq.Name);
                        }

                        writer.Close();
                    }

                }
                catch (Exception e) { 
                    MessageBox.Show("Sem Permissão para acessaer este Diretório!",
                                    "Atenção",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation,
                                    MessageBoxDefaultButton.Button1);
                }

            }
        }

        private void btnSalvarNomeArquivo_Click(object sender, EventArgs e)
        {
            SaveNameArquivos();
        }
    }
}
